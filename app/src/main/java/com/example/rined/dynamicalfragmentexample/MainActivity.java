package com.example.rined.dynamicalfragmentexample;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends Activity {
    Fragm1 fr1;
    Fragm2 fr2;
    Fragm3 fr3;
    FragmentTransaction frTrans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fr1=new Fragm1();
        fr2=new Fragm2();
        fr3=new Fragm3();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v){
        frTrans=getFragmentManager().beginTransaction();

        switch(v.getId()){
            case R.id.but1:
                frTrans.replace(R.id.fraglayout, fr1);
                break;
            case R.id.but2:
                frTrans.replace(R.id.fraglayout, fr2);
                break;
            case R.id.but3:
                frTrans.replace(R.id.fraglayout, fr3);
                break;
        }

        frTrans.commit();


    }
}
